(function($) {

  $.fn.MediaBox = function (options) {
    var mediaUploader;

    var defaults = {
      submit_btn_label: 'Upload media',
      upload_btn_label: 'Choose media',
      frame_label: 'Choose media',
      type: 'image'
    };

    return this.each(function() {
      var settings = $.extend({}, defaults, options);

        $(this).click(function(e) {
          e.preventDefault();
          // If the uploader object has already been created, reopen the dialog
          if (mediaUploader) {
            mediaUploader.open();
            return;
          }
          // Extend the wp.media object
          mediaUploader = wp.media.frames.file_frame = wp.media({
            title: settings.frame_label,
            button: {
              text: settings.upload_btn_label
            }, multiple: false,
            library: {type: settings.type}
          });

          // When a file is selected, grab the URL and set it as the text field's value
          mediaUploader.on('select', function() {
            attachment = mediaUploader.state().get('selection').first().toJSON();
            console.log(attachment);
            $('#image-url').val(attachment.url);
          });
          // Open the uploader dialog
          mediaUploader.open();

          function render_preview(type) {
            switch (type) {
              case 'image':
                break;

              case 'video':
                break;
            }
          }
      });
    });
  }
}) (jQuery);

jQuery('.media-upload').MediaBox();
