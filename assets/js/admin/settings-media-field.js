/**
 * Widget Image Field Javascript
 */
var mediaField;

jQuery(document).ready(function ($) {
  mediaField = {
    // Call this from the upload button to initiate the upload frame.
    uploader: function (widget_id, media_type) {

      media_type = media_type ? media_type : 'image';

      var frame = wp.media({
        title: customField.frame_title,
        multiple: false,
        library: {type: media_type},
        button: {text: customField.button_title}
      });

      // Handle results from media manager.
      frame.on('close', function () {
        var attachments = frame.state().get('selection').toJSON();

        if (attachments.length != 0) {
          mediaField.render(widget_id, attachments[0]);

          var element = $('#' + widget_id);

          $('.upload-settings-media', element).on('click', function () {
            mediaField.uploader(element.attr('id'), element.data('type'));
          });
        }
      });

      frame.open();

      return false;
    },

    // Output Image preview and populate widget form.
    render: function (widget_id, attachment) {
      if (attachment.type == 'video') {
        $('#preview-' + widget_id).html(mediaField.videoHTML(attachment));
      } else if(attachment.type == 'image') {
        $('#preview-' + widget_id).html(mediaField.imgHTML(attachment, widget_id));
      } else {
        $('#preview-' + widget_id).html(mediaField.archiveHTML(attachment, widget_id));
      }

      $('.remove-media', '#' + widget_id).live('click', function(){ mediaField.remove(widget_id); });

      if (attachment) {
        $('#attachment-' + widget_id).val(attachment.id);
      }
    },

    // Render html for the image.
    imgHTML: function (attachment) {
      var img_html = '';
      img_html += '<img class="upload-settings-media" src="' + attachment.url + '"';
      if (attachment.alt != '') { img_html += 'alt="' + attachment.alt + '" '; }
      img_html += '/>';
      img_html += '<span class="dashicons dashicons-no remove-media"></span>';

      return img_html;
    },

    // Render html for the video.
    videoHTML: function (attachment, widget_id) {
      var video_html = '';
      $('img','#' +  widget_id).addClass('hidden');
      video_html += '<video controls width="300" height="170">';
      video_html +=   '<source src="' + attachment.url + '" type="' + attachment.mime + '">';
      video_html += '</video>';
      video_html += '<span class="dashicons dashicons-no remove-media"></span>';

      return video_html;
    },

    // Render html for the archive.
    archiveHTML: function (attachment, widget_id) {
      var archive_html = '';
      archive_html += '<img class="default-icon upload-settings-media"';
      archive_html += 'src="' + $('#' + widget_id).data('default-basic-img') + '"/>';
      archive_html += '<span class="dashicons dashicons-no remove-media"></span><br/>';
      archive_html += '<a target="_blank" href="' + attachment.url + '">';
      archive_html += 'Preview ';
      archive_html += attachment.title + '</a>';

      return archive_html;
    },
    remove: function(widget_id) {
      $('#attachment-' + widget_id).val('');
      $('#preview-' + widget_id).html('<img class="default-icon upload-settings-media" src="' + $('#' + widget_id).data('default-img') + '" alt="Settings media"/>');
      $('.upload-settings-media', '#' + widget_id).live('click', function(){ mediaField.uploader(widget_id, $('#' + widget_id).data('type')) });
    }
  };

  $('.media-uploader').each(function () {
    var element = $(this);

    $('.upload-settings-media', element).on('click', function () {
      mediaField.uploader(element.attr('id'), element.data('type'));
    });
    $('.remove-media', element).on('click', function () {
      mediaField.remove(element.attr('id'));
    });
  });
});

