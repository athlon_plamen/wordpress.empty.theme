<?php

/**
 * Widget for Display WidgetName.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 *
 */
class WidgetName extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'widget-name',
            __('WidgetName', 'ThemeName'),
            array('description' => __('Athlon widget description', 'ThemeName'),)
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
        echo $args['before_widget'];

        include 'widget.html.php';

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        include 'widget-admin.html.php';
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;

        $instance['title'] = $new_instance['title'];

        return $instance;
    }
}