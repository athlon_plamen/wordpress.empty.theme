<p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_attr_e('Title', 'ThemeName'); ?>:</label><br/>
    <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?>" value="<?php echo $instance['title'] ?>" size="25"/>
</p>
