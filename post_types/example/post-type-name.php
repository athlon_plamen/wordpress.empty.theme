<?php
/**
 * Create custom PostTypeName
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 */

/* Creation of the custom post type */
add_action('init', 'createPostTypeName');
function createPostTypeName() {
    $labels = array(
        'name'                  => __('PostTypeName', 'ThemeName'),
        'singular_name'         => __('PostTypeName', 'ThemeName'),
        'add_new'               => __('Add New PostTypeName', 'ThemeName'),
        'add_new_item'          => __('Add New PostTypeName', 'ThemeName'),
        'edit_item'             => __('Edit PostTypeName', 'ThemeName'),
        'new_item'              => __('New PostTypeName', 'ThemeName'),
        'all_items'             => __('All PostTypeName', 'ThemeName'),
        'view_item'             => __('View PostTypeName', 'ThemeName'),
        'search_items'          => __('Search PostTypeName', 'ThemeName'),
        'not_found'             => __('No PostTypeName found', 'ThemeName'),
        'not_found_in_trash'    => __('No PostTypeName found in Trash', 'ThemeName'),
        'archives'              => __('Post Archives', 'ThemeName'),
        'insert_into_item'      => __('insert into post', 'ThemeName'),
        'uploaded_to_this_item' => __('Uploaded to this post', 'ThemeName'),
        'featured_image'        => __('Featured Image', 'ThemeName'),
        'set_featured_image'    => __('Set featured image', 'ThemeName'),
        'use_featured_image'    => __('Use as featured image', 'ThemeName'),
        'menu_name'             => __('PostTypeName'),
        'filter_items_list'     => null, /* String for the table views hidden heading */
        'items_list_navigation' => null, /* String for the table pagination hidden heading */
        'items_list'            => null, /* String for the table hidden heading */
        'parent_item_colon'     => null,
        'name_admin_bar'        => __('PostTypeName', 'ThemeName'),
    );

    register_post_type('PostTypeSlug',
        array(
            'label'                 => $labels['name'],
            'labels'                => $labels,
            'description'           => '',
            'public'                => true,
            'capability_type'       => 'post',
            //'capabilities'          => array('edit_post', 'read_post', 'delete_post'),
            'supports'              => array('title', 'excerpt', 'editor', 'thumbnail'),
            'rewrite'               => array('slug' => 'PostTypeSlug'),
            'taxonomies'            => array('TaxonomySlug'),
            'menu_icon'             => null, /* See https://developer.wordpress.org/resource/dashicons/#dashboard */
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'map_meta_cap'          => null,
            'register_meta_box_cb'  => null,
            'query_var'             => true,
            'show_in_nav_menus'     => true,
            'has_archive'           => true,
            'menu_position'         => null,
            'hierarchical'          => false,
            'can_export'            => true,
            'show_in_rest'          => false,
            'permalink_epmask'      => EP_PERMALINK,
            'rest_base'             => '',
            'rest_controller_class' => '',
        )
    );
}

/* Hook into the init action to add taxonomy when it fires */
add_action('init', 'createTaxonomyName');
function createTaxonomyName() {
    /* Add new taxonomy, make it hierarchical (like categories) */
    $labels = array(
        'name'              => __('TaxonomyName', 'ThemeName'),
        'singular_name'     => __('TaxonomyName', 'ThemeName'),
        'search_items'      => __('Search TaxonomyName', 'ThemeName'),
        'all_items'         => __('All TaxonomyName', 'ThemeName'),
        'edit_item'         => __('Edit TaxonomyName', 'ThemeName'),
        'update_item'       => __('Update TaxonomyName', 'ThemeName'),
        'view_item'         => __('View TaxonomyName', 'ThemeName'),
        'add_new_item'      => __('Add TaxonomyName', 'ThemeName'),
        'new_item_name'     => __('New TaxonomyName', 'ThemeName'),
        'menu_name'         => __('TaxonomyName', 'ThemeName'),
        'parent_item'       => __('Parent Category', 'ThemeName'),
        'parent_item_colon' =>  __('Parent Category:', 'ThemeName'),
    );

    register_taxonomy('TaxonomySlug', 'PostTypeSlug', array(
        'label'                 => $labels['name'],
        'description'           => __('', 'ThemeName'),
        'labels'                => $labels,
        'hierarchical'          => true,
        'public'                => true,
        'show_tagcloud'         => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_quick_edit'    => true,
        'show_in_nav_menus'     => false,
        'meta_box_cb'           => false,
        'show_admin_column'     => true,
        'query_var'             => true,
        'sort'                  => true,
        'rewrite'               => array('slug' => 'TaxonomySlug'),
        'capabilities'          => array('manage_terms'),
        'update_count_callback' => '_update_post_term_count',
    ));
}

/**
 * PostTypeName update messages.
 *
 * See /wp-admin/edit-form-advanced.php
 *
 * @param array $messages Existing post update messages.
 *
 * @return array Amended post update messages with new CPT update messages.
 */
add_filter('post_updated_messages', 'codexPostTypeNameUpdateMessages');
function codexPostTypeNameUpdateMessages($messages) {
    $post             = get_post();
    $post_type        = get_post_type($post);
    $post_type_object = get_post_type_object($post_type);

    $messages['PostTypeSlug'] = array(
        0  => '', /* Unused. Messages start at index 1. */
        1  => __('PostTypeName updated.', 'ThemeName'),
        2  => __('Custom field updated.', 'ThemeName'),
        3  => __('Custom field deleted.', 'ThemeName'),
        4  => __('PostTypeName updated.', 'ThemeName'),
        /* translators: %s: date and time of the revision */
        5  => isset($_GET['revision']) ? sprintf(__('PostTypeName restored to revision from %s', 'ThemeName'), wp_post_revision_title((int)$_GET['revision'], false)) : false,
        6  => __('PostTypeName published.', 'ThemeName'),
        7  => __('PostTypeName saved.', 'ThemeName'),
        8  => __('PostTypeName submitted.', 'ThemeName'),
        9  => sprintf(
            __('PostTypeName scheduled for: <strong>%1$s</strong>.', 'ThemeName'),
            /* translators: Publish box date format, see http://php.net/date */
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date))
        ),
        10 => __('PostTypeName draft updated.', 'ThemeName'),
    );


    if ($post_type_object->publicly_queryable && isset($messages[$post_type]) && $post_type == 'PostTypeSlug') {
        $permalink = get_permalink($post->ID);

        $view_link = sprintf(' <a href="%s">%s</a>', esc_url($permalink), __('View PostTypeName', 'ThemeName'));
        $messages[$post_type][1] .= $view_link;
        $messages[$post_type][6] .= $view_link;
        $messages[$post_type][9] .= $view_link;

        $preview_permalink = add_query_arg('preview', 'true', $permalink);
        $preview_link      = sprintf(' <a target="_blank" href="%s">%s</a>', esc_url($preview_permalink), __('Preview PostTypeName', 'ThemeName'));
        $messages[$post_type][8] .= $preview_link;
        $messages[$post_type][10] .= $preview_link;
    }

    return $messages;
}

/**
 * Register Custom Meta-boxes.
 */
$metaboxName = new WPAlchemy_MetaBox(array
(
    'id'       => 'metabox_name',
    'title'    => 'metabox_name',
    'types'    => array('PostTypeSlug'),
    'context'  => 'side',
    'priority' => 'default',
    'template' => TEMPLATE_PATH . '/post_types/PostTypeName/metaboxes/metabox_name.php',
    'autosave' => true,
));

/**
 *  Manage custom post type columns.
 *
 * @param $columns
 *
 * @return array
 */
add_filter('manage_PostTypeSlug_posts_columns', 'PostTypeNameManageColumns');
function PostTypeNameManageColumns($columns) {

    $new_columns = array(
        'thumbnail' => __('Thumbnail'),
    );

    return array_merge($columns, $new_columns);
}

/**
 * Add sortable columns.
 *
 * @param $columns
 *
 * @return mixed
 */
add_filter('manage_edit-PostTypeSlug_sortable_columns', 'addSortableColumnsToPostTypeName');
function addSortableColumnsToPostTypeName($columns) {
    $columns['title'] = 'title';

    return $columns;
}

/**
 * Manage custom post type columns.
 *
 *
 * @var $column_name
 * @return  mixed
 */
add_action('manage_PostTypeSlug_posts_custom_column', 'managePostTypeNameColumns', 10, 2);
function managePostTypeNameColumns($column_name) {
    global $post;

    switch ($column_name) {
        case 'thumbnail':
            echo get_the_post_thumbnail($post->ID, array(80, 80));
            break;
    }
}