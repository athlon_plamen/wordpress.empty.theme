<?php
/**
 * WP Theme Public settings.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 */

/**
 * Register required assets.
 */
add_action('init', 'ThemeNameLoadAssets');
function ThemeNameLoadAssets() {
    if (! is_admin()) {
        
        /**
         * Register Styles.
         * Example use: wp_register_style( $handle, $src, $deps, $ver, $media );
         */
        // wp_register_style( 'checkbox', ASSETS_URL . '/css/libs/jquery.checkBox.css' );
        // wp_register_style( 'selectbox', ASSETS_URL . '/css/libs/jquery.selectbox.css' );
        // wp_register_style( 'form.elements', false, array('checkbox', 'selectbox') );
        
        /**
         * Register Scripts.
         * Example use: wp_register_script( $handle, $src, $deps, $ver, $in_footer );
         */
        wp_deregister_script('jquery');
        wp_register_script('jquery', ASSETS_URL . '/js/libs/jquery-1.9.1.min.js', false, '1.9.1', ASSETS_JAVASCRIPTS_IN_FOOTER);
        
        wp_register_script('init', ASSETS_URL . '/js/init.js', array('jquery'), false, ASSETS_JAVASCRIPTS_IN_FOOTER);
    }
}