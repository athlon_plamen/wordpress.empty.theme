<?php
/**
 * Register settings options.
 *
 * @package    WordPress 4.5
 * @subpackage avanti
 * @author     Athlon
 *
 */

if (! class_exists('SettingsApiHelper')) {
    class SettingsApiHelper
    {
        /**
         * Settings sections array.
         *
         * @var array
         */
        private $settingsSections = array();
        
        /**
         * Settings fields array.
         *
         * @var array
         */
        private $settingsFields = array();
        
        /**
         * Set settings sections.
         *
         * @param array $sections setting sections array
         */
        public function setSections($sections) {
            $this->settingsSections = $sections;
        }
        
        /**
         * Set settings fields.
         *
         * @param array $fields settings fields array
         */
        public function setFields($fields) {
            $this->settingsFields = $fields;
        }
        
        /**
         * Initialize and registers the settings sections and fields to WordPress.
         *
         * Usually this should be called at `adminInit` hook.
         *
         * This function gets the initiated settings sections and fields. Then
         * registers them to WordPress and ready for use.
         */
        public function adminInit() {
            /* Register settings sections */
            foreach ($this->settingsSections as $section) {
                if (isset($section['id'])) {
                    if (false === get_option($section['id'])) {
                        add_option($section['id']);
                    }
                    
                    add_settings_section(
                        $section['id'],
                        $section['title'],
                        '__return_false',
                        $section['id']);
                }
            }
            
            /* Register settings fields */
            foreach ($this->settingsFields as $section => $field) {
                foreach ($field as $option) {
                    $args = array(
                        'id'      => $option['name'],
                        'desc'    => isset($option['desc']) ? $option['desc'] : '',
                        'name'    => $option['label'],
                        'section' => $section,
                        'size'    => isset($option['size']) ? $option['size'] : null,
                        'options' => isset($option['options']) ? $option['options'] : '',
                        'std'     => isset($option['default']) ? $option['default'] : '',
                    );
                    
                    add_settings_field(
                        $section . '[' . $option['name'] . ']',
                        $option['label'],
                        array($this, $this->cameLize('callback_' . $option['type'])),
                        $section,
                        $section,
                        $args);
                }
            }
            
            /* Creates our settings in the options table*/
            foreach ($this->settingsSections as $section) {
                if (isset($section['id'])) {
                    $validate = $section['validate'] ? array($this, $this->cameLize('settings_validation_' . $section['validate'])) : '';
                    register_setting($section['id'], $section['id'], $validate);
                }
            }
        }
        
        /**
         * Displays a text field for a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackText($args) {
            $value       = esc_attr($this->getOption($args['id'], $args['section'], $args['std']));
            $size        = isset($args['size']) && ! is_null($args['size']) ? $args['size'] : 'regular';
            $placeholder = isset($args['options']['placeholder']) && ! empty($args['options']['placeholder']) ? $args['options']['placeholder'] : '';
            $class       = isset($args['options']['class']) && ! empty($args['options']['class']) ? $args['options']['class'] : '';
            
            $html = sprintf(
                '<input type="text" class="%1$s-text %6$s" id="%2$s[%3$s]" name="%2$s[%3$s]" value="%4$s" placeholder="%5$s"/>',
                $size,
                $args['section'],
                $args['id'],
                $value,
                $placeholder,
                $class);
            
            $html .= sprintf(
                '<span class="description"> %s</span>',
                $args['desc']);
            
            echo $html;
        }
        
        /**
         * Displays a checkbox for a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackCheckbox($args) {
            $value = esc_attr($this->getOption($args['id'], $args['section'], $args['std']));
            
            $html = sprintf(
                '<input type="checkbox" class="checkbox" id="%1$s[%2$s]" name="%1$s[%2$s]" value="on"%4$s />',
                $args['section'], $args['id'],
                $value,
                checked($value, 'on', false));
            
            $html .= sprintf(
                '<label for="%1$s[%2$s]"> %3$s</label>',
                $args['section'],
                $args['id'],
                $args['desc']);
            
            echo $html;
        }
        
        /**
         * Displays a multicheckbox a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackMulticheck($args) {
            $value = $this->getOption($args['id'], $args['section'], $args['std']);
            
            $html = '';
            foreach ($args['options'] as $key => $label) {
                $checked = isset($value[$key]) ? $value[$key] : '0';
                $html .= sprintf(
                    '<input type="checkbox" class="checkbox" id="%1$s[%2$s][%3$s]" name="%1$s[%2$s][%3$s]" value="%3$s"%4$s />',
                    $args['section'], $args['id'],
                    $key,
                    checked($checked, $key, false));
                
                $html .= sprintf(
                    '<label for="%1$s[%2$s][%4$s]"> %3$s</label><br>',
                    $args['section'],
                    $args['id'],
                    $label,
                    $key);
            }
            
            $html .= sprintf(
                '<span class="description"> %s</label>',
                $args['desc']);
            
            echo $html;
        }
        
        /**
         * Displays a multi radio boxes a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackRadio($args) {
            $value = $this->getOption($args['id'], $args['section'], $args['std']);
            
            $html = '';
            foreach ($args['options'] as $key => $label) {
                $html .= sprintf(
                    '<input type="radio" class="radio" id="%1$s[%2$s][%3$s]" name="%1$s[%2$s]" value="%3$s"%4$s />',
                    $args['section'],
                    $args['id'],
                    $key,
                    checked($value, $key, false));
                
                $html .= sprintf(
                    '<label for="%1$s[%2$s][%4$s]"> %3$s</label><br>',
                    $args['section'], $args['id'],
                    $label,
                    $key);
            }
            
            $html .= sprintf(
                '<span class="description"> %s</label>',
                $args['desc']);
            
            echo $html;
        }
        
        /**
         * Displays a selectbox for a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackSelect($args) {
            $value = esc_attr($this->getOption($args['id'], $args['section'], $args['std']));
            $size  = isset($args['size']) && ! is_null($args['size']) ? $args['size'] : 'regular';
            
            $html = sprintf(
                '<select class="%1$s" name="%2$s[%3$s]" id="%2$s[%3$s]">',
                $size,
                $args['section'],
                $args['id']);
            
            foreach ($args['options'] as $key => $label) {
                $html .= sprintf(
                    '<option value="%s"%s>%s</option>',
                    $key,
                    selected($value, $key, false),
                    $label);
            }
            
            $html .= sprintf('</select>');
            $html .= sprintf(
                '<span class="description"> %s</span>',
                $args['desc']);
            
            echo $html;
        }
        
        /**
         * Displays a textarea for a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackTextarea($args) {
            $value = esc_textarea($this->getOption($args['id'], $args['section'], $args['std']));
            $size  = isset($args['size']) && ! is_null($args['size']) ? $args['size'] : 'regular';
            $desc  = isset($args['desc']) ? $args['desc'] : '';
            
            $html = sprintf(
                '<textarea rows="5" cols="55" class="%1$s-text" id="%2$s[%3$s]" name="%2$s[%3$s]">%4$s</textarea>',
                $size,
                $args['section'],
                $args['id'],
                $value);
            
            $html .= sprintf(
                '<br><span class="description"> %s</span>',
                $desc);
            
            echo $html;
        }
        
        /**
         * Displays a textarea as text editor for a settings field.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackEditor($args) {
            global $wp_editor_default_settings;
            $rows  = isset($args['options']['rows']) ? $args['options']['rows'] : 35;
            $value = self::getOption($args['id'], $args['section']);
            
            $local_wp_editor_settings = array(
                'textarea_name' => "{$args['section']}[{$args['id']}]",
                'textarea_rows' => $rows,
            );
            
            $wp_editor_default_settings = array_merge($wp_editor_default_settings, $local_wp_editor_settings);
            
            if (isset($args['options']['tinymce'])) {
                $wp_editor_default_settings = array_merge($wp_editor_default_settings, array('tinymce' => $args['options']['tinymce']));
            }
            
            wp_editor(stripslashes($value), $args['id'], $wp_editor_default_settings);
        }
        
        /**
         * Displays a upload image button.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackImage($args) {
            wp_enqueue_media();
            wp_enqueue_script('custom-settings-media-uploader', get_template_directory_uri() . '/assets/js/admin/settings-media-field.js', array('jquery', 'media-upload', 'media-views'));
            
            $desc         = isset($args['desc']) ? $args['desc'] : '';
            $button_name  = isset($args['options']['button_name']) && ! empty($args['options']['button_name']) ? $args['options']['button_name'] : 'Select image';
            $insert_label = isset($args['options']['insert_label']) && ! empty($args['options']['insert_label']) ? $args['options']['insert_label'] : 'Insert image';
            $value        = self::getOption($args['id'], $args['section']);
            $default_img  = ASSETS_URL . '/img/admin/add_image.png';
            $img_src      = $default_img;
            $remove_icon  = '';
            $default_cls  = ' default-icon';
            
            if (! empty($value) && is_numeric($value)) {
                $img_src = wp_get_attachment_image_url($value, 'medium');
                $remove_icon = '<span class="dashicons dashicons-no remove-media"></span>';
                $default_cls = '';
            }
            
            wp_localize_script('custom-settings-media-uploader', 'customField', array(
                'frame_title'  => $button_name,
                'button_title' => $insert_label,
            ));
            
            $html = sprintf(
                '<span class="description"> %s</span><br/>',
                $desc);
            
            $html .= sprintf(
                '<div class="media-uploader" id="%2$s-%3$s" data-default-img="%6$s" data-type="image">
                <div class="image_preview" id="preview-%2$s-%3$s">
                <img class="upload-settings-media%8$s" src="%4$s" alt="Settings media"/>%7$s</div>
                <input type="button" class="button upload-settings-media" value="%1$s"/>
                <input type="hidden" id="attachment-%2$s-%3$s" name="%2$s[%3$s]" value="%5$s"/></div>',
                $button_name,
                $args['section'],
                $args['id'],
                $img_src,
                $value,
                $default_img,
                $remove_icon,
                $default_cls
            );
            
            echo $html;
        }
        
        /**
         * Displays a upload video button.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackVideo($args) {
            wp_enqueue_media();
            wp_enqueue_script('custom-settings-media-uploader', get_template_directory_uri() . '/assets/js/admin/settings-media-field.js', array('jquery', 'media-upload', 'media-views'));
            
            $desc         = isset($args['desc']) ? $args['desc'] : '';
            $button_name  = isset($args['options']['button_name']) && ! empty($args['options']['button_name']) ? $args['options']['button_name'] : 'Select video';
            $insert_label = isset($args['options']['insert_label']) && ! empty($args['options']['insert_label']) ? $args['options']['insert_label'] : 'Insert video';
            $value        = self::getOption($args['id'], $args['section']);
            $default_img  = ASSETS_URL . '/img/admin/add_video.png';
            $remove_icon  = '';
            $video_src    = '';
            $hide_default_img = ' default-icon';
            
            if (! empty($value) && is_numeric($value)) {
                $mime_type        = get_post_mime_type($value);
                $video_src        = '<video controls width="300" height="170"><source src="' . wp_get_attachment_url($value)  . '" type="' . $mime_type . '"></video>';
                $remove_icon      = '<span class="dashicons dashicons-no remove-media"></span>';
                $hide_default_img = ' hidden';
            }
            
            wp_localize_script('custom-settings-media-uploader', 'customField', array(
                'frame_title'  => $button_name,
                'button_title' => $insert_label,
            ));
            
            $html = sprintf(
                '<span class="description"> %s</span><br/>',
                $desc);
            
            $html .= sprintf(
                '<div class="media-uploader" id="%2$s-%3$s" data-default-img="%6$s" data-type="video">
                <div class="video_preview" id="preview-%2$s-%3$s">%5$s
                <img class="upload-settings-media%7$s" src="%6$s" alt="Settings video placeholder"/>%8$s</div>
                <input type="button" class="button upload-settings-media" value="%1$s"/>
                <input type="hidden" id="attachment-%2$s-%3$s" name="%2$s[%3$s]" value="%4$s"/></div>',
                $button_name,
                $args['section'],
                $args['id'],
                $value,
                $video_src,
                $default_img,
                $hide_default_img,
                $remove_icon
            );
            
            echo $html;
        }
        
        /**
         * Displays a upload file asset.
         *
         * @param array $args settings field args
         *
         * @return mixed
         */
        public function callbackArchive($args) {
            wp_enqueue_media();
            wp_enqueue_script('custom-settings-media-uploader', get_template_directory_uri() . '/assets/js/admin/settings-media-field.js', array('jquery', 'media-upload', 'media-views'));
            
            $desc         = isset($args['desc']) ? $args['desc'] : '';
            $button_name  = isset($args['options']['button_name']) && ! empty($args['options']['button_name']) ? $args['options']['button_name'] : 'Select file';
            $insert_label = isset($args['options']['insert_label']) && ! empty($args['options']['insert_label']) ? $args['options']['insert_label'] : 'Insert file';
            $value        = self::getOption($args['id'], $args['section']);
            $default_img  = ASSETS_URL . '/img/admin/add_file.png';
            $default_basic_img  = ASSETS_URL . '/img/admin/add_file.png';
            $file_src = $default_img;
            $attached_file_link = '';
            $remove_icon  = '';
            
            if (! empty($value) && is_numeric($value)) {
                $default_img  = ASSETS_URL . '/img/admin/attach_file.png';
                $file_src     = wp_get_attachment_url($value);
                $attached_file_link = '<a href="' . wp_get_attachment_url($value) . '" target="_blank">Preview ' . get_the_title($value) . '</a>';
                $remove_icon      = '<span class="dashicons dashicons-no remove-media"></span>';
            }
            
            wp_localize_script('custom-settings-media-uploader', 'customField', array(
                'frame_title'  => $button_name,
                'button_title' => $insert_label,
            ));
            
            $html = sprintf(
                '<span class="description"> %s</span><br/>',
                $desc);
            
            $html .= sprintf(
                '<div class="media-uploader" id="%2$s-%3$s" data-default-img="%9$s" data-default-basic-img="%6$s" data-type="zip">
                <div class="zip_preview" id="preview-%2$s-%3$s">
                <img width="100" height="100" class="upload-settings-media" src="%6$s" alt="Settings zip placeholder"/>%8$s<br/>%7$s</div>
                <input type="button" class="button upload-settings-media" value="%1$s"/>
                <input type="hidden" id="attachment-%2$s-%3$s" name="%2$s[%3$s]" value="%4$s"/></div>',
                $button_name,
                $args['section'],
                $args['id'],
                $value,
                $file_src,
                $default_img,
                $attached_file_link,
                $remove_icon,
                $default_basic_img
            );
            
            echo $html;
        }
        /**
         * Url validation function.
         *
         * @param $input
         *
         * @return mixed
         */
        public function settingsValidationUrl($input) {
            $output = array();
            foreach ($input as $key => $value) {
                if (strpos($key, 'url')) {
                    $validate = preg_match('/^(http|https):\/\/([a-z0-9-]\.)*/i', $value);
                    if (! $validate && ! empty($value)) {
                        $output[$key] = 'http://' . $value;
                    } else {
                        $output[$key] = $value;
                    }
                } else {
                    $output[$key] = $value;
                }
            }
            
            return apply_filters('settingsValidationUrl', $output, $input);
        }
        
        /**
         * Email validation function.
         *
         * @param $input
         *
         * @return mixed
         */
        public function settingsValidationEmail($input) {
            $output = array();
            foreach ($input as $key => $value) {
                if (strpos($key, 'email')) {
                    $validate = is_email($value);
                    if (! $validate && ! empty($value)) {
                        add_settings_error('options', 'settings_updated', 'Invalid email', 'error');
                    } else {
                        $output[$key] = $value;
                    }
                } else {
                    $output[$key] = $value;
                }
            }
            
            return apply_filters('settingsValidationEmail', $output, $input);
        }
        
        /**
         * General validation function.
         *
         * @param $input
         *
         * @return mixed
         */
        public function settingsValidationPresence($input) {
            $output = array();
            foreach ($input as $key => $value) {
                if (strpos($key, 'email') || $key === 'email') {
                    $validate = is_email($value);
                    if (! $validate && ! empty($value)) {
                        add_settings_error($key, 'settings_updated', 'Invalid email', 'error');
                    } else {
                        $output[$key] .= $value;
                    }
                } elseif (strpos($key, 'url') || $key === 'url') {
                    $validate = preg_match('/^(http|https):\/\/([a-z0-9-]\.)*/i', $value);
                    if (! $validate && ! empty($value)) {
                        $output[$key] = 'http://' . $value;
                    } else {
                        $output[$key] .= $value;
                    }
                } elseif (strpos($key, 'required')) {
                    if (empty($value)) {
                        add_settings_error($key, 'settings_updated', 'Field is required', 'error');
                    } else {
                        $output[$key] .= $value;
                    }
                } else {
                    $output[$key] = $value;
                }
            }
            
            return apply_filters('settingsValidationPresence', $output, $input);
        }
        
        public function cameLize($input, $separator = '_') {
            if (phpversion() >= '5.4.32') {
                return str_replace($separator, '', ucwords($input, $separator));
            }else {
                return str_replace(' ' , '', ucwords(str_replace($separator, ' ', $input)));
            }
            
        }
        
        /**
         * Get the value of a settings field.
         *
         * @param string $option  settings field name
         * @param string $section the section name this field belongs to
         * @param string $default default text if it's not found
         *
         * @return string
         */
        function getOption($option, $section, $default = '') {
            $options = get_option($section);
            if (isset($options[$option])) {
                return $options[$option];
            }
            
            return $default;
        }
        
        /**
         * Show navigation as tabs.
         *
         * Shows all the settings section labels as tab.
         */
        function showNavigation() {
            $html = '<h2 class="nav-tab-wrapper">';
            
            foreach ($this->settingsSections as $tab) {
                $html .= sprintf('<a href="#%1$s" class="nav-tab" id="%1$s-tab">%2$s</a>', $tab['id'], $tab['title']);
            }
            
            $html .= '</h2>';
            
            echo $html;
        }
        
        /**
         * Show the section settings forms.
         *
         * This function displays every sections in a different form.
         */
        function showForms() {
            ?>
            <div class="metabox-holder">
                <?php foreach ($this->settingsSections as $form) : ?>
                    <div id="<?php echo $form['id']; ?>" class="group">
                        <form method="post" action="options.php" id="post">
                            <?php if (isset($form['id'])) : ?>
                                
                                <?php settings_fields($form['id']); ?>
                                <?php do_settings_sections($form['id']); ?>
                                
                                <?php submit_button(); ?>
                            
                            <?php endif; ?>
                        </form>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php $this->script(); ?>
        <?php }
        
        /**
         * JavaScript codes.
         *
         * This code uses local-storage for displaying active tabs.
         */
        function script() {
            ?>
            <script type="text/javascript">
              jQuery(function () {
                jQuery('.group').hide();
                var activetab = '';
                if (typeof(localStorage) != 'undefined') {
                  activetab = localStorage.getItem("activetab");
                }
                if (activetab != '' && jQuery(activetab).length) {
                  jQuery(activetab).fadeIn();
                } else {
                  jQuery('.group:first').fadeIn();
                }
                jQuery('.group .collapsed').each(function () {
                  jQuery(this).find('input:checked').parent().parent().parent().nextAll().each(
                    function () {
                      if (jQuery(this).hasClass('last')) {
                        jQuery(this).removeClass('hidden');
                        return false;
                      }
                      jQuery(this).filter('.hidden').removeClass('hidden');
                    });
                });

                if (activetab != '' && jQuery(activetab + '-tab').length) {
                  jQuery(activetab + '-tab').addClass('nav-tab-active');
                }
                else {
                  jQuery('.nav-tab-wrapper a:first').addClass('nav-tab-active');
                }
                jQuery('.nav-tab-wrapper a').click(function (e) {
                  jQuery('.nav-tab-wrapper a').removeClass('nav-tab-active');
                  jQuery(this).addClass('nav-tab-active').blur();
                  var clicked_group = jQuery(this).attr('href');
                  if (typeof(localStorage) != 'undefined') {
                    localStorage.setItem("activetab", jQuery(this).attr('href'));
                  }
                  jQuery('.group').hide();
                  jQuery(clicked_group).fadeIn();
                  e.preventDefault();
                });
              });
            </script>
            <?php
        }
    }
}
