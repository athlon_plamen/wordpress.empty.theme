<?php
/**
 * Utility functions.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 */

/**
 * Truncates the given string to the given length.
 *
 * @param string $string sdsfsdf
 * @param int    $max
 * @param string $suffix
 *
 * @return string
 */
function utilTruncateString($string, $max = 20, $suffix = '') {
    if (strlen($string) <= $max) {
        return $string;
    }

    return substr($string, 0, strrpos(substr($string, 0, $max), ' ')) . ' ' . $suffix;
}

/**
 * Flattens the ids returned from $wpdb->get_results and casts the values to INT.
 *
 * @param $array
 *
 * @return array|bool
 */
function flattenPostIds($array) {
    if (! is_array($array)) {
        return false;
    }

    $result = array();

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, flattenPostIds($value));
        } else {
            $result[$key] = (int)$value;
        }
    }

    return $result;
}

/**
 * Outputs pagination links.
 *
 * @return void
 */
function wpAthPagination() {
    global $wp_query, $wp_rewrite;
    $a = array();

    /* Count of total pages */
    $a['total'] = $wp_query->max_num_pages;

    if ($a['total'] > 1) {
        /* Defaults current */
        if (! $current = get_query_var('paged')) {
            $current = 1;
        }

        if (! empty($wp_query->query_vars['s'])) {
            $a['add_args'] = array('s' => get_query_var('s'));
        }

        $a['base']      = @add_query_arg('paged', '%#%');
        $a['current']   = $current; /* what is the current page number */
        $a['mid_size']  = 5; /* how many links to show on the left and right of the current */
        $a['end_size']  = 1; /* how many links to show in the beginning and end */
        $a['type']      = 'array'; /* what is the type of output: plain/array/list */
        $a['prev_text'] = '&laquo; Previous'; /* text of the "Previous page" link */
        $a['next_text'] = 'Next &raquo;'; /* text of the "Next page" link */

        echo '<div class="navigation"><span class="pages">More trips: </span>', implode('', paginate_links($a)), '</div>';
    }
}

/**
 * Gets the terms associated to this post.
 *
 * @param $_post_id
 *
 * @return array Array('my_taxonomy' => array([term Object1], [term Object2])); [term Object] holds the info for the term
 */
function getPostTaxonomyTerms($_post_id = false) {
    if ($_post_id != false) {
        $_post_taxonomy_terms = array();

        /* Determine the post type */
        $_post_type = get_post_type($_post_id);

        /* Get the taxonomies registered for this post type */
        $_post_taxonomies = get_object_taxonomies($_post_type);

        /* For each taxonomy collect the terms for this post */
        foreach ($_post_taxonomies as $taxonomy) {
            $_temp_terms = array();
            $_temp_terms = get_the_terms(get_the_ID(), $taxonomy);

            if ($_temp_terms && ! is_wp_error($_temp_terms)) {
                $_post_taxonomy_terms[$taxonomy] = $_temp_terms;
            }
        }

        return $_post_taxonomy_terms;
    } else {
        return false;
    }
}

/**
 * Prepares the link elements of post's taxonomy terms.
 *
 * @uses get_post_taxonomy_terms
 *
 * @param $_post_id
 *
 * @return array Array('name' => 'My terms', htef='http://example.com/{taxonomy_slug}/{term_slug}')
 */
function preparePostTermsLinks($_post_id = false) {
    if ($_post_id == false) {
        $_post_id = get_the_ID();
    }

    $_post_terms = getPostTaxonomyTerms($_post_id);
    if ($_post_terms != false) {
        $_the_links = array();
        foreach ($_post_terms as $taxonomy) {
            foreach ($taxonomy as $term) {
                $_the_links[] = array('name' => $term->name, 'href' => get_term_link($term->slug, $term->taxonomy));
            }
        }

        return $_the_links;
    } else {
        return array();
    }
}

/**
 * Triggers custom action "Load Breadcrumbs".
 */
function breadcrumbsAction() {
    do_action('loadBreadcrumbs');
}

/**
 * Outputs Breadcrumbs
 *
 * @return void
 */
/* Add custom action for "Load Breadcrumbs" */
add_action('loadBreadcrumbs', 'theBreadcrumbs');
function theBreadcrumbs() {
    $text_format      = '<span>%1$s</span>';
    $text_last_format = '<span class="last">%1$s</span>';
    $link_format      = '<a href="%1$s" title="%2$s">%2$s</a>';

    echo '<div id="breadcrumb_data">';

    if (is_home()) {
        echo sprintf($text_last_format, get_bloginfo('name'));
    } else {
        $term = get_queried_object();

        echo sprintf($link_format, get_option('home'), get_bloginfo('name'));

        if (is_page()) {
            echo sprintf($text_last_format, $term->post_title);
        } else {
            if (is_404()) {
                echo sprintf($text_last_format, __('404 - Something went wrong !'));
                exit;
            }

            $post_type_obj = get_post_type_object(get_post_type());

            /* Display current post type */
            if (is_single() || is_tax()) {
                echo sprintf($link_format, get_bloginfo('url') . '/' . $post_type_obj->rewrite['slug'], $post_type_obj->labels->name);
            } else {
                echo sprintf($text_last_format, $post_type_obj->labels->name);
            }

            /* Display current taxonomy */
            if (is_tax()) {
                $taxonomy = get_taxonomy($term->taxonomy);

                echo sprintf($text_format, $taxonomy->labels->name);
                echo sprintf($text_last_format, $term->name);
            }

            /* Display current title */
            if (is_single()) {
                echo sprintf($text_last_format, $term->post_title);
            }
        }

        echo '</div>';
    }
}

/**
 * Gets current page slug.
 */
function theSlug() {
    global $post;

    $post_data = get_post($post->ID, ARRAY_A);
    $slug      = $post_data['post_name'];

    return $slug;
}