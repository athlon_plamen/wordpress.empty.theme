<?php
/**
 * Functions and Definitions.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 */

/**
 * Constants.
 */
include_once 'config/constants.php';

/**
 * Meta-boxes.
 */
include_once 'includes/metaboxes/setup.php';

/**
 * Utilities.
 */
include_once 'includes/utility.php';
include_once 'includes/settings-api-helper.php';

/**
 * Custom Post Types.
 */
// include('post_types/example/post-type-name.php');

/**
 * Options.
 */
include('options/general/contact_us.php');
//include('options/general/disclaimer.php');

/**
 * Custom Widgets.
 */
include('widgets/widget-name/widget-name.php');

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
add_action('after_setup_theme', 'ThemeNameSetup');
function ThemeNameSetup() {
    
    /* This theme styles the visual editor with editor-style.css to match the theme style. */
    add_editor_style();
    
    /* This theme uses post thumbnails */
    add_theme_support('post-thumbnails');
    add_image_size('thumbnail', 80, 80, true);
    
    /* Add default posts and comments RSS feed links to head */
    add_theme_support('automatic-feed-links');
    
    /* Register menus */
    register_nav_menus(array(
        'primary' => __('Primary Navigation', 'ThemeName'),
    ));
}

/**
 * Register widgetized areas.
 *
 * @uses register_sidebar
 */
add_action('widgets_init', 'ThemeNameWidgetsInit');
function ThemeNameWidgetsInit() {
    
    /* Global Widget Area - Displayed on all pages */
    register_sidebar(array(
        'name'          => __('Global', 'ThemeName'),
        'id'            => 'global-widget-area',
        'description'   => __('Global widget area', 'ThemeName'),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'before_text'   => '',
        'after_text'    => '',
    ));
    
    register_widget('WidgetName');
}

/**
 * Include Theme config.
 */
include 'config/admin.php';
include 'config/public.php';
