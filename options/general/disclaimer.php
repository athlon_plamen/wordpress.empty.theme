<?php

/**
 * Disclaimer option page.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 */
if (! class_exists('DisclaimerSettingsPage')) {
    class DisclaimerSettingsPage
    {
        /**
         * @var SettingsApiHelper
         */
        private $settingsApi;
        
        /**
         * ContactInfoSettingsPage constructor.
         */
        function __construct() {
            $this->settingsApi = new SettingsApiHelper;
            
            add_action('admin_init', array($this, 'adminInit'));
            add_action('admin_menu', array($this, 'adminMenu'));
        }
        
        /**
         * Initial fields and sections.
         */
        function adminInit() {
            $this->settingsApi->setSections($this->getSettingsSection());
            $this->settingsApi->setFields($this->getSettingsFields());
            
            $this->settingsApi->adminInit();
        }
        
        /**
         * Create a custom menu page.
         */
        function adminMenu() {
            global $disclaimerPageHook;
            
            $disclaimerPageHook = add_options_page(
                'Disclaimer',
                'Disclaimer',
                'manage_options',
                'disclaimerSettingsSection',
                array($this, 'disclaimerSettingsForm'));
            
            add_action('load-' . $disclaimerPageHook, array($this, 'addNewDisclaimerHelpTabs'));
        }
        
        /**
         * Register sections.
         *
         * @return array
         */
        function getSettingsSection() {
            $section = array(
                array(
                    'id'       => 'disclaimer_settings_section',
                    'title'    => __('Disclaimer', 'ThemeName'),
                    'validate' => '',
                ),
            );
            
            return $section;
        }
        
        /**
         * Register fields.
         *
         * @return array
         */
        function getSettingsFields() {
            $fields = array(
                'disclaimer_settings_section' => array(
                    array('name' => 'disclaimer_title', 'label' => __('Disclaimer title', 'ThemeName'), 'type' => 'text'),
                    array('name' => 'disclaimer_description', 'label' => __('Disclaimer description', 'ThemeName'), 'type' => 'editor'),
                ),
            );
            
            return $fields;
        }
        
        /**
         * Register custom Help tabs.
         */
        function addNewDisclaimerHelpTabs() {
            global $disclaimerPageHook;
            $screen = get_current_screen();
            
            /**
             * Check if current screen is My Admin Page.
             * Don't add help tab if it's not.
             */
            if ($screen->id != $disclaimerPageHook) {
                return;
            }
            
            /* Add overview tab if current screen is My Admin Page */
            $screen->add_help_tab(array(
                'id'      => 'overview',
                'title'   => __('Overview', 'ThemeName'),
                'content' => '<p>' . __('On this you can enter the content of disclaimer pop-up.', 'ThemeName') . '</p>',
            ));
            
            $screen->set_help_sidebar('<p><strong>' . __('For more information', 'ThemeName') . '</strong>:</p>');
        }
        
        /**
         * Shows Settings form.
         */
        function disclaimerSettingsForm() {
            $this->settingsApi->showForms();
        }
    }
    
    new DisclaimerSettingsPage();
    
}