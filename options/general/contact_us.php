<?php
/**
 * Registers settings section and fields.
 *
 * @package    WordPress 4.5
 * @subpackage ThemeName
 * @author     Athlon
 */
if (! class_exists('ContactInfoSettingsPage')) {
    class ContactInfoSettingsPage {
        /**
         * @var SettingsApiHelper
         */
        private $settingsApi;
        
        /**
         * ContactInfoSettingsPage constructor.
         */
        function __construct() {
            $this->settingsApi = new SettingsApiHelper;
            
            add_action('admin_init', array($this, 'adminInit'));
            add_action('admin_menu', array($this, 'adminMenu'));
        }
        
        /**
         * Initial fields and sections.
         */
        function adminInit() {
            $this->settingsApi->setSections($this->getSettingsSections());
            $this->settingsApi->setFields($this->getSettingsFields());
            
            $this->settingsApi->adminInit();
        }
        
        /**
         * Create a custom menu page.
         */
        function adminMenu() {
            add_options_page(
                'Contact us',
                'Contact us',
                'manage_options',
                'contact-info-section',
                array($this, 'contactUsSettingsForm'));
        }
        
        /**
         * Register sections.
         *
         * @return array
         */
        function getSettingsSections() {
            $section = array(
                array(
                    'id'       => 'contact-info-section',
                    'title'    => __('Contact Settings', 'ThemeName'),
                    'validate' => '',
                ),
            );
            
            return $section;
        }
        
        /**
         * Register fields.
         *
         * @return array
         */
        function getSettingsFields() {
            $fields = array(
                'contact-info-section' => array(
                    array(
                        'name'  => 'phone_number',
                        'label' => __('Telephone number', 'ThemeName'),
                        'desc'  => __('Enter telephone number', 'ThemeName'),
                        'type'  => 'text',
                    ),
                    array(
                        'name'  => 'email',
                        'label' => __('E-mail', 'ThemeName'),
                        'desc'  => __('Enter e-mail', 'ThemeName'),
                        'type'  => 'text',
                    ),
                    array(
                        'name'  => 'address',
                        'label' => __('Address', 'ThemeName'),
                        'desc'  => __('Enter Address', 'ThemeName'),
                        'type'  => 'textarea',
                    ),
                ),
            );
            
            return $fields;
        }
        
        /**
         * Shows Settings form.
         */
        function contactUsSettingsForm() {
            $this->settingsApi->showForms();
        }
    }
    
    new ContactInfoSettingsPage();
}

