<?php get_header(); ?>
    <div id="main">
    <div id="content">
        <div class="post">
            <h2 class="title">404 - Something went wrong !</h2>
            <div class="entry">
                <p>We're sorry. The Web address you entered is not a functioning page on our website.</p>
                <p><a href="<?php bloginfo('url'); ?>">Click here to go to the homepage</a></p>
            </div>
        </div>
    </div>
<?php get_footer();
